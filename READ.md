# LICENSE
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2018 aoiica

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
  
# コードの解説

- 運用番号がぐるぐるするプログラムです
- 色々importしてますが、結局標準ライブラリしか利用してません
- 時間の更新はtkinterのmisc遅延で実装
    - apscheduler使おうとしたけど並列がうまくいかなかったため
- 藤沢鎌倉間を36分割して1分ごとにリストの中に入った変数に一を足すことで位置を表現しています
    - 藤沢からの所要時間と捉えるて36分割
    - 1足して位置を表現するために藤沢を0として、位置は負の値もとります
- 8時から20時以外のデータはなくその時間に動かすとエラー吐きます
- 状態黒->状態灰は5分、状態灰->状態黒は7分らしいですがどっちも6分で作っちゃいました
    - そのうち時刻表見て治します
- 今後実装しようかと考えているのは
    - ツイッターから運用取ってきてぶっこむ
    - nowMIn/Hourの変数を任意にいじれるようにした物を作る
    - Javascriptに書き直してブラウザで見れるようにする
- 作業中一番絶望を感じたのはelectronがpythonじゃなくてJavascriptで動かすものだと思い出した時です
